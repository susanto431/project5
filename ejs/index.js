const express = require('express');
const app = express();

app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.render('index')
});

// Path Parameter (lbh umum dipakai)
// app.get('/:id', (req, res) => {
//     const parameter = req.params.id
//     console.log(parameter)

//     res.render('index')
// });

// app.get('/product/:namaProduct', (req, res) => {
//     const parameter = req.params.namaProduct

//     if (parameter == "laptop") {
//         const namaProduct = parameter
//         const product = "Laptop ROG"
//         res.render('product', { namaProduct, product })
//     } else if (parameter == "bangku") {
//         const namaProduct = parameter
//         const product = "Bangku Elektronik"
//         res.render('product', { namaProduct, product })
//     }

//     res.render(parameter)
// });

// cara baru degnan sistem query
// Logic busine untuk searching ke database
app.get('/product', (req, res) => {
    const namaProduct = req.query.namaProduct
    const product = req.query.product

    res.render('product', { namaProduct, product })
});

app.get('/greet/:name', (req, res) => {
    const parameter = req.params.name
    console.log(parameter)

    res.render('greet', { parameter })
});

// kaku. 
app.get('/coba', (req, res) => {
    res.render('index');
});

app.listen(3000, () => {
    console.log(`Server started on 3000`);
});

// pastebin/:id