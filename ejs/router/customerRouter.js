const express = require('express');
const route = express.Router();


// Object => Customer

// Butuh API yg bisa request utk membuat / create sebuah produk
route.post('/customer', (req, res) => {
    res.send("Data customer Posted")
});
// Butuh API yg bisa request utk melihat semua data produk
route.get('/customer', (req, res) => {
    res.send("All Data customer")
});
// Butuh API yg bisa request utk mengubah semua data produk
route.put('/customer', (req, res) => {
    res.send("Mengubah Data customer")
});
// Butuh API yg bisa request utk mendelete semua data produk
route.delete('/customer', (req, res) => {
    res.send("Mendelete Data customer")
});

// entitas / action => customer/login
route.post('/customer/login', (req, res) => {
    return
})

route.post('/customer/order', (req, res) => {
    return
})

module.exports = route