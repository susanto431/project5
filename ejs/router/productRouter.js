const express = require('express');
const router = express.Router();


// Object => Product
// CRUD => BASIC FUNCTION
// CREATE
// READ
// UPDATE
// DELETE

let dataProduct = [
    {
        namaProduct: "Meja",
        jumlahProduct: 12
    },
    {
        namaProduct: "Bangku",
        jumlahProduct: 15
    },
    {
        namaProduct: "Gelas",
        jumlahProduct: 150
    }
]

// Butuh API yg bisa request utk membuat / create sebuah produk
router.post('/product', (req, res) => {

    // cara 1 deklarasi
    // const namaProduct = req.body.namaProduct
    // const jumlahProduct = req.body.jumlahProduct

    // cara 2 deklarasi (Destruktif method) => nama body kaku.
    const { namaProduct, jumlahProduct } = req.body

    // bagian validasi 
    if (namaProduct === undefined || jumlahProduct === undefined) {
        res.status(400).send("Bad Request")
        return
    }

    console.log(namaProduct)
    console.log(jumlahProduct)

    dataProduct.push({
        namaProduct: namaProduct,
        jumlahProduct: jumlahProduct
    })

    res.send("Inserted")

});
// Butuh API yg bisa request utk melihat semua data produk
router.get('/product', (req, res) => {
    res.json(dataProduct)
});

// Butuh API yg bisa request utk mengubah semua data produk
router.put('/product', (req, res) => {
    res.send("Mengubah Data Product")
});
// Butuh API yg bisa request utk mendelete semua data produk
router.delete('/product', (req, res) => {
    res.send("Mendelete Data Product")
});

module.exports = router